<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'bbva-automotriz' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'FoX`}(I`XGBuE*35E;(f!;lRAVSZ:/|5Sq7nGKzP~w@Yv|^M_0Pl?L*~U&$dz9bE' );
define( 'SECURE_AUTH_KEY',  'BnXq(A4$D>1LZ8whLv9)E 2tuB`*T~u0id)9YE>xeSOsR^lS*.;R=(TIM,rCV@]1' );
define( 'LOGGED_IN_KEY',    '6/)>)Onh,91sa[Vx2X _qbx}:x@hiYE=`+AgrI-hKK+C1a{M,raF6l#ye]njZ^Hq' );
define( 'NONCE_KEY',        ')B{<ozQ=L%1<)VC3@#/Xg6uFPbTMvG0)7<h.6+7zo-N_:Bwb}}t5q=2#G9U,7=*a' );
define( 'AUTH_SALT',        '4Mmx.a}*{Mr1!I_&J1Czg[@XjSQ&Rn<s(3qWStqn6s|<DozI1.iC186[]O9FHu+w' );
define( 'SECURE_AUTH_SALT', 'I+aB#QB^;2[>wL6zWyEW|p KX1Nmi43ab-nBgqqS&mK`xBJ>(c;FGcfYv~}ur8F?' );
define( 'LOGGED_IN_SALT',   'PNGEXsy9rBJE^bP??qRlIb9+fllfP+M}cWh  RjCo.yU`lPB`mp30&`ngg;Y$hqn' );
define( 'NONCE_SALT',       'ZDH&z>Qpm~6uH|o)5xBGK2uQW0iu6ameuRCRWq,NPLf!Nq66lH^e%53`4X>U%++n' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
