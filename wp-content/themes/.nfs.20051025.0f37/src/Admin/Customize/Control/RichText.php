<?php

namespace OpenWeb\Admin\Customize\Control;

class RichText extends \WP_Customize_Control
{
    public $type = 'textarea';

    public function enqueue()
    {
        add_action('admin_enqueue_scripts', function () {
            wp_enqueue_script('openweb-customize-rich-text', trailingslashit(get_template_directory_uri()) . '/src/Admin/js/customize-rich-text.js', array('jquery'), null, true);
        });
    }

    public function render_content()
    {
        $settings = [
            'media_buttons' => false,
            'quicktags' => false
        ];

        $this->filterEditor();
?>
        <?php if (! empty( $this->label)): ?>
        <label>
            <span class="customize-control-title"><?php echo esc_html($this->label); ?></span>
        </label>
        <?php endif; ?>

        <?php if (! empty($this->description)): ?>
        <span class="description customize-control-description"><?php echo $this->description; ?></span>
        <?php endif; ?>

        <?php wp_editor($this->value(), $this->id, $settings ); ?>

        <?php

        do_action('admin_print_footer_scripts');
    }

    private function filterEditor()
    {
        add_filter('the_editor', function($output) {
            return preg_replace('/<textarea/', '<textarea '.$this->get_link(), $output, 1);
        });
    }
}
