<?php

/**
 * @todo rehacer pensando en el theme no como script dependiente del proyecto
 */

require_once __DIR__.'/../../../../wp-blog-header.php';
require_once __DIR__.DIRECTORY_SEPARATOR.'custom.php';

function getChildren($data, $parentId) {
    $output = [];

    foreach ($data as $item) {
        if ($item->parent_id == $parentId) {
            $output[] = [
                'alias' => $item->comment_author,
                'author' => $item->comment_author,
                'author_email' => $item->comment_author_email,
                'date' => \DateTime::createFromFormat('Y-m-d H:i:s', $item-> comment_date)->format('YmdHis000'),
                'content' => $item->comment_content,
                'replies' => getChildren($item->get_children(), $item->parent_id)
            ];
        }
    }
    return $output;
}

$args = [
    'post_type' => ['post', 'page'],
    'post_status' => 'publish',
    'posts_per_page' => -1
];

$query = new \WP_Query($args);

$posts = $query->get_posts();

$json = [];

foreach ($posts as $post) {

    $url = parse_url(get_permalink($post->ID), PHP_URL_PATH);

    $commentsArgs = [
        'post_id' => $post->ID,
        'status' => 'approve',
        'hierarchical' => 'threaded'
    ];

    $comments = get_comments($commentsArgs);

    foreach ($comments as $comment) {
        $json[$url][] = [
            'alias' => $comment->comment_author,
            'author' => $comment->comment_author,
            'author_email' => $comment->comment_author_email,
            'date' => \DateTime::createFromFormat('Y-m-d H:i:s', $comment-> comment_date)->format('YmdHis000'),
            'content' => $comment->comment_content,
            'replies' => getChildren($comment->get_children(), '')
        ];
    }
}

file_put_contents('openweb-comments.json', json_encode($json));