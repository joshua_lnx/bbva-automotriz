<?php
/**
 * Template Name: Openweb - Page Empty
 * Template Post Type: page
 * Template Description: Plantilla para páginas sólo con cabecera y pie.
 *
 * @package OpenWeb
 * @subpackage Coronita
 * @since OpenWeb Coronita 1.0
 */

get_header();
?>

<?php
while (have_posts()) {
    the_post();
}
?>

<section class="container-fluid">
    <div class="container">
        <?php echo the_content(); ?>
    </div>
</section>

<?php get_footer();
