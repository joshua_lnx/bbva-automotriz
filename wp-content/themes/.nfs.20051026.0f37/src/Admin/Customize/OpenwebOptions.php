<?php

namespace OpenWeb\Admin\Customize;

use OpenWeb\Admin\Customize;
use OpenWeb\Admin\Customize\Control\MultipleCheckbox;
use OpenWeb\Admin\Customize\Control\RichText;
use OpenWeb\Admin\Post;

class OpenwebOptions extends Customize
{
    const REGENERATE_SEARCH = 'regenerate-search';

    const REGENERATE_NONCE_SEARCH = self::REGENERATE_SEARCH.'-openweb';
    
    const REGENERATE_LAZY_LOAD = 'regenerate-lazy-load';

    const REGENERATE_NONCE_LAZY_LOAD = self::REGENERATE_LAZY_LOAD.'-openweb';


    protected function init()
    {
        add_action('admin_enqueue_scripts', function () {
            wp_enqueue_script('openweb-customize', get_template_directory_uri() . '/src/Admin/js/customize-openweb.js', ['jquery','customize-preview']);
        });

        add_action('wp_ajax_'.self::REGENERATE_SEARCH, [$this, 'regenerateSearchJson']);
        add_action('wp_ajax_'.self::REGENERATE_LAZY_LOAD, [$this, 'regenerateLazyLoadJson']);
    }

    protected function createPanel($customize)
    {
        $this->customize = $customize;

        $this->customize->add_panel('openweb_options', [
            'title' => __('Opciones OpenWeb', 'openweb'),
            'priority' => 211,
            'capability' => 'edit_theme_options',
        ]);

        $this->createSearchSection();
        $this->createSecuritySection();
        $this->createCookiesPolicySection();
        $this->createLazyLoadSection();
    }

    private function createSearchSection()
    {
        $this->customize->add_section('openweb_search_section' , [
            'title' => __('Buscador OpenWeb', 'openweb'),
            'panel' => 'openweb_options',
            'priority' => 1,
            'capability' => 'edit_theme_options',
        ]);

        $this->generateSearchSettings();
    }

    private function generateSearchSettings()
    {
        $this->customize->add_setting('openweb_platform_cloudsearch', [
            'type' => 'theme_mod',
            'default'  => false,
            'capability' => 'edit_theme_options',
        ]);
        $this->customize->add_setting('openweb_platform_cloudsearch_results', [
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
        ]);
        $this->customize->add_setting('openweb_platform_cloudsearch_content_types', [
            'type' => 'option',
            'capability' => 'edit_theme_options',
            'sanitize_callback' => [$this, 'sanitizeContentTypes'],
        ]);
        
        $this->generateSearchControls();
    }
    
    private function generateSearchControls()
    {
        $this->customize->add_control('openweb_platform_cloudsearch', [
            'label'    => __('Buscador Cloudsearch', 'openweb'),
            'section'  => 'openweb_search_section',
            'priority' => 1,
            'type'     => 'checkbox',
            'description' => __('La plataforma usa el servicio de AWS CloudSearch para las búsquedas, marcando la casilla se añadirán automáticamente los componentes visuales necesarios, recuerda que más abajo puedes definir los contenidos a indexar automáticamente.', 'openweb')
        ]);

        $this->customize->add_control('openweb_platform_cloudsearch_results', [
            'label'    => __('Página de resultados', 'openweb'),
            'section'  => 'openweb_search_section',
            'priority' => 2,
            'description' => __('Asocia la página de resultados que hayas creado en el apartado páginas.', 'openweb'),
            'type'     => 'dropdown-pages'
        ]);

        $this->customize->add_control('openweb_json_search', array(
            'type' => 'button',
            'settings' => [],
            'priority' => 3,
            'section' => 'openweb_search_section',
            'label' => __('Regeneración de los contenidos', 'openweb'),
            'description' => __('Regeneración de todos los ficheros JSON dedicados a la indexación para el buscador', 'openweb'),
            'input_attrs' => [
                'value' => __('Regeneración JSON', 'openweb'),
                'class' => 'button button-primary',
                'id' => 'openweb-json-search',
                'data-confirm' => __('¿Quieres regenerar los JSON del buscador?', 'openweb'),
                'data-nonce' => wp_create_nonce(self::REGENERATE_NONCE_SEARCH)
            ],
        ));

        $types = get_post_types();
        $choices = array_combine(array_keys($types), array_map(function($val) { return ucwords(str_replace('_', ' ', $val)); }, array_values($types)));

        $this->customize->add_control(
            new MultipleCheckbox($this->customize, 'openweb_platform_cloudsearch_content_types', [
                'label' => __('Contenidos a indexar', 'openweb'),
                'section'  => 'openweb_search_section',
                'priority' => 4,
                'description' => __('Asocia las tipos de contenidos definidos en Wordpress para su indexación.', 'openweb'),
                'choices' => $choices
            ])
        );
    }

    private function createSecuritySection()
    {
        $this->customize->add_section('openweb_security_section' , [
            'title' => __('Zona Privada', 'openweb'),
            'panel' => 'openweb_options',
            'priority' => 2,
            'capability' => 'edit_theme_options',
        ]);

        $this->generateSecuritySettings();
    }

    private function generateSecuritySettings()
    {
        $this->customize->add_setting('openweb_platform_security', [
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
        ]);

        $this->customize->add_setting('openweb_platform_security_register', [
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
        ]);

        $this->customize->add_setting('openweb_platform_security_redirect', [
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
        ]);
        
        $this->generateSecurityControls();
    }
    
    private function generateSecurityControls()
    {
        $this->customize->add_control('openweb_platform_security', [
            'label' => __('Zona privada', 'openweb'),
            'section' => 'openweb_security_section',
            'priority' => 1,
            'type' => 'checkbox',
            'description' => __('La plataforma ofrece la capacidad de tener un área privada para usuarios con Armadillo'.PHP_EOL.', esta opción es solo para añadir la visualización en el tema.', 'openweb')
        ]);

        $this->customize->add_control('openweb_platform_security_register', [
            'label'    => __('Página de registro', 'openweb'),
            'section'  => 'openweb_security_section',
            'priority' => 2,
            'description' => __('Asocia la página de registro de usuarios que hayas creado en el apartado páginas.', 'openweb'),
            'type'     => 'dropdown-pages'
        ]);
        $this->customize->add_control('openweb_platform_security_redirect', [
            'label'    => __('Página de redirección', 'openweb'),
            'section'  => 'openweb_security_section',
            'priority' => 3,
            'description' => __('Asocia la página a la que redirige una vez que un usuario se ha autenticado.', 'openweb'),
            'type'     => 'dropdown-pages'
        ]);
    }

    public function sanitizeContentTypes($values)
    {
        $multi = ! is_array($values) ? explode(',', $values) : $values;

        return ! empty($multi) ? array_map('sanitize_text_field', $multi) : array();
    }

    private function createCookiesPolicySection()
{
    $this->customize->add_section('openweb_cookies_policy_section', [
        'title' => __('Política de cookies', 'openweb'),
        'panel' => 'openweb_options',
        'priority' => 3,
        'capability' => 'edit_theme_options',
    ]);

    $this->generateCookiesPolicySettings();
}

    private function generateCookiesPolicySettings()
    {
        $this->customize->add_setting('openweb_show_cookies_policy', [
            'type' => 'theme_mod',
            'default' => false,
            'capability' => 'edit_theme_options',
        ]);
        $this->customize->add_setting('openweb_cookies_policy_title', [
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
        ]);
        $this->customize->add_setting('openweb_cookies_policy_text', [
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
        ]);

        $this->generateCookiesPolicyControls();
    }

    private function generateCookiesPolicyControls()
    {
        $this->customize->add_control('openweb_show_cookies_policy', [
            'label' => __('Mostrar política de cookies', 'openweb'),
            'section' => 'openweb_cookies_policy_section',
            'priority' => 1,
            'type' => 'checkbox',
        ]);

        $this->customize->add_control('openweb_cookies_policy_title', [
            'label' => __('Título política de cookies', 'openweb'),
            'section' => 'openweb_cookies_policy_section',
            'priority' => 2,
            'type' => 'text',
        ]);


        $this->customize->add_control(
            new RichText($this->customize, 'openweb_cookies_policy_text', [
                'label' => __('Texto política de cookies', 'openweb'),
                'section' => 'openweb_cookies_policy_section',
                'priority' => 3
            ])
        );
    }

    private function createLazyLoadSection()
    {
        $this->customize->add_section('openweb_lazy_load_section', [
            'title' => __('Utilidades Lazy-load', 'openweb'),
            'panel' => 'openweb_options',
            'priority' => 4,
            'capability' => 'edit_theme_options',
        ]);
        
        $this->generateLazyloadControls();
    }

    private function generateLazyloadControls()
    {
        $this->customize->add_control('openweb_json_lazy_load', array(
            'type' => 'button',
            'settings' => [],
            'priority' => 1,
            'section' => 'openweb_lazy_load_section',
            'label' => __('Regeneración JSON - LAZY LOAD', 'openweb'),
            'description' => __('Regeneración de todos los ficheros JSON necesarios para la carga de contenidos mediante AJAX: home, categorías, etiquetas y autores', 'openweb'),
            'input_attrs' => [
                'value' => __('Regeneración JSON', 'openweb'),
                'class' => 'button button-primary',
                'id' => 'openweb-json-lazy-load',
                'data-confirm' => __('¿Quieres regenerar JSON para la carga mediante AJAX?', 'openweb'),
                'data-nonce' => wp_create_nonce(self::REGENERATE_NONCE_LAZY_LOAD)
            ],
        ));
    }

    public function regenerateSearchJson()
    {
        if ((isset($_POST['action']) && self::REGENERATE_SEARCH === $_POST['action'])
            &&
            (isset($_POST['regenerate']) && wp_create_nonce(self::REGENERATE_NONCE_SEARCH) === $_POST['regenerate'])
        ) {
            $types = get_option('openweb_platform_cloudsearch_content_types');

            $contents = empty($types) ? Post::METABOXES_TYPES : $types;

            $args = [
                'post_type' => $contents,
                'post_status' => 'publish',
                'posts_per_page' => -1
            ];

            $query = new \WP_Query($args);

            $posts = $query->get_posts();

            $total = 0;

            foreach ($posts as $post) {
                $json = Post::generateSearchJson($post);

                $meta = get_post_custom($post->ID);

                $dir = isset($meta['openweb-privateIndex']) ? Post::PATH_SEARCH_PRIVATE_JSON : Post::PATH_SEARCH_JSON;

                $name = (in_array($post->post_type, Post::METABOXES_TYPES) ? 'post' : $post->post_type).'_'.$post->ID.'_wp-search.json';

                Post::removeSearchJson($name);
                Post::saveJson($name, $dir, $json);
                ++$total;
            }

            wp_die(json_encode(['count' => $total]));
        }
    }

    public function regenerateLazyLoadJson()
    {
        if ((isset($_POST['action']) && self::REGENERATE_LAZY_LOAD === $_POST['action'])
            &&
            (isset($_POST['regenerate']) && wp_create_nonce(self::REGENERATE_NONCE_LAZY_LOAD) === $_POST['regenerate'])
        ) {
            Post::createHomeJson();

            $vars = [
                'post_type' => ['post'],
                'post_status' => 'publish',
                'posts_per_page' => -1
            ];

            $query = new \WP_Query($vars);
            foreach ($query->get_posts() as $post) {
                $json = Post::generatePostJson($post);
                Post::saveJson('post_'.$post->ID.'.json', Post::PATH_LAZY_LOAD_JSON.DIRECTORY_SEPARATOR.Post::PATH_POST, $json);
                Post::createCategoriesJson($post->ID);
                Post::createTagsJson($post->ID);
                Post::createAuthorJson($post->post_author);
            }

            wp_die(json_encode(['count' => 'Regenerated: HOME, CATS, TAGS, AUTHORS']));
        }
    }
}
