

<?php

/*
* Template Name: Plantilla Negocio Promotor
*/
 get_header();



// Optener la pagina Actual





 $categoria1 = 'negocio';
 $taxonomia1 = 'Categoria_negocio';
 $termino1 = 'credito-puente';
    $args1 = array(
        'post_type' => $categoria1, // the post type
        'posts_per_page'=>'30',

        'tax_query' => array(
            array(
                'taxonomy' => $taxonomia1, // the custom vocabulary
                'field'    => 'slug',
                'terms'    => array($termino1),      // provide the term slugs
            ),
        ),
    );



$categoria2 = 'negocio';
$taxonomia2 = 'Categoria_negocio';
$termino2 = 'prepuente';
  $args2 = array(
        'post_type' => $categoria2, // the post type
        'posts_per_page'=>'30',

        'tax_query' => array(
            array(
                'taxonomy' => $taxonomia2, // the custom vocabulary
                'field'    => 'slug',
                'terms'    => array($termino2),      // provide the term slugs
            ),
        ),
    );




$categoria3 = 'negocio';
$taxonomia3 = 'Categoria_negocio';
$termino3 = 'reserva-territorial';
  $args3 = array(
        'post_type' => $categoria3, // the post type
        'posts_per_page'=>'30',

        'tax_query' => array(
            array(
                'taxonomy' => $taxonomia3, // the custom vocabulary
                'field'    => 'slug',
                'terms'    => array($termino3),      // provide the term slugs
            ),
        ),
    );


$categoria4 = 'negocio';
$taxonomia4 = 'Categoria_negocio';
$termino4 = 'urbanizacion-e-infraestructura';
  $args4 = array(
        'post_type' => $categoria4, // the post type
        'posts_per_page'=>'30',

        'tax_query' => array(
            array(
                'taxonomy' => $taxonomia4, // the custom vocabulary
                'field'    => 'slug',
                'terms'    => array($termino4),      // provide the term slugs
            ),
        ),
    );



?>



<?php get_header(); ?>


    <div class="display-none-mobil">


        <div class="hero-banner2">
            <img src="<?php echo get_the_post_thumbnail_url (); ?>">
        </div>
        <section>
            <div class="container-fluid bg-primary display-none-mobil">
                <div class="container my-5">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="content-text">
                                <?php if (have_rows('negocio')) :
                                    while (have_rows('negocio')) : the_row();
                                        // vars
                                        $titulo01 = get_sub_field('titulo01');
                                        $imagen01 = get_sub_field('imagen01');

                                        $titulo02 = get_sub_field('titulo02');
                                        $imagen02 = get_sub_field('imagen02');

                                        $titulo03 = get_sub_field('titulo03');
                                        $imagen03 = get_sub_field('imagen03');

                                        $titulo04 = get_sub_field('titulo04');
                                        $imagen04 = get_sub_field('imagen04');
                                ?>
                                <img class="my-6" src="<? echo get_sub_field('imagen01')?>" height="24px" width="24px" style="margin-top: 30px;">
                                <h2 class="thin"> <br><?php echo $titulo01; ?></h2>
                            </div>
                        </div>
                        <div class="col-sm-9 white mx-6">
                            <div class="bg-white">
                                <div class="card-body my-3 bbva-cards"></div>
                                <h6 class="card-title mx-4 mb-4 h6 bullet categoria-9 ":after> Documentos</h6>
                                <ul class="scroll">
                                    <?php
                                        $query1 = new WP_Query ( $args1);
                                        if ( $query1->have_posts() ) :
                                    ?>
                                    <div class="documento">
                                        <ul class="scroll">
                                            <?php while ( $query1->have_posts() ) : $query1->the_post();
                                                $descargadoc = get_field('descarga');
                                                $descargaenlace = get_field('enlace_descarga');
                                                if( !empty($descargadoc) ):
                                                    $descarga = $descargadoc;
                                                    else:
                                                        $descarga = $descargaenlace;
                                                endif
                                            ?>
                                            <?php if  (!empty($descargadoc || $descargaenlace)):?>
                                            <li>
                                                <?php the_title(); ?>
                                                <a href="<?php echo $descarga; ?>"target="_blank">
                                                    <i class="bbva-coronita_download pl10 descarga"></i>
                                                </a>
                                            </li>
                                                <?php endif; ?>
                                                <?php
                                                endwhile; ?>
                                        </ul>
                                    </div> <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



        <section>
            <div class="container-fluid bg-primary ">

                <div class="container my-5">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="content-text">
                            <img class="my-6" src="<? echo get_sub_field('imagen02')?>" height="24px" width="24px" style="margin-top: 30px;">

                                <h2 class="thin"> <br><?php echo $titulo02; ?></h2>


                            </div>
                        </div>
                        <div class="col-sm-9 white mx-6">
                            <div class="bg-white">
                            <div class="card-body my-3 bbva-cards"></div>
                            <h6 class="card-title mx-4 mb-4 h6 bullet categoria-9 ":after>Documentos</h6>
                                <ul class="scroll">
                            <?php

                                $query2 = new WP_Query ( $args2);
                                    if ( $query2->have_posts() ) :
                                    ?>
                                        <div class="documento"><ul>
                                            <?php while ( $query2->have_posts() ) : $query2->the_post();


                                        $descargadoc = get_field('descarga');

                                        $descargaenlace = get_field('enlace_descarga');


                                        if( !empty($descargadoc) ):

                                        $descarga = $descargadoc;

                                        else:
                                        $descarga = $descargaenlace;

                                        endif
                                    ?>

                                <?php if  (!empty($descargadoc || $descargaenlace)):?>

                                <li><?php the_title();

                                    ?>
                                    <a href="<?php echo $descarga; ?>"target="_blank"><i class="bbva-coronita_download pl10 descarga"></i></a></li>
                                    <?php endif; ?>
                                    <?php

                                    endwhile; ?>
                                    </ul>
                                    </div> <?php endif; ?>

                                        </ul>

                    </div></div>

                        </div>



                    </div>
                </div>


        </section>



        <section>
            <div class="container-fluid bg-primary ">

                <div class="container my-5">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="content-text">
                            <img class="my-6" src="<? echo get_sub_field('imagen03')?>" height="24px" width="24px" style="margin-top: 30px;">

                                <h2 class="thin"> <br><?php echo $titulo03; ?></h2>


                            </div>
                        </div>
                        <div class="col-sm-9 white mx-6">
                            <div class="bg-white">
                            <div class="card-body my-3 bbva-cards"></div>

                            <h6 class="card-title mx-4 mb-4 h6 bullet categoria-9 ":after> Documentos</h6>
                            <ul class="scroll">

                                <?php

                                $query3 = new WP_Query ( $args3);
                                    if ( $query3->have_posts() ) :
                                    ?>
                                        <div class="documento"><ul>
                                            <?php while ( $query3->have_posts() ) : $query3->the_post();


                                        $descargadoc = get_field('descarga');

                                        $descargaenlace = get_field('enlace_descarga');


                                        if( !empty($descargadoc) ):

                                        $descarga = $descargadoc;

                                        else:
                                        $descarga = $descargaenlace;

                                        endif
                                    ?>

                                <?php if  (!empty($descargadoc || $descargaenlace)):?>

                                <li><?php the_title();

                                    ?>
                                    <a href="<?php echo $descarga; ?>"target="_blank"><i class="bbva-coronita_download pl10 descarga"></i></a></li>
                                    <?php endif; ?>
                                    <?php

                                    endwhile; ?>
                                   </ul> </div> <?php endif; ?>

                                        </ul>



                        </div></div>


                        </div>

                    </div>
                </div>


        </section>

        <section>
            <div class="container-fluid bg-primary ">

                <div class="container my-5">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="content-text">
                            <img class="my-6" src="<? echo get_sub_field('imagen04')?>" height="24px" width="24px" style="margin-top: 30px;">

                                <h2 class="thin"> <br><?php echo $titulo04; ?></h2>


                            </div>
                        </div>
                        <div class="col-sm-9 white mx-6">
                            <div class="bg-white">
                            <div class="card-body my-3 bbva-cards"></div>

                            <h6 class="card-title mx-4 mb-4 h6 bullet categoria-9 ":after> Documentos</h6>
                            <ul class="scroll">
                                <?php

                                $query4 = new WP_Query ( $args4);
                                    if ( $query4->have_posts() ) :
                                    ?>
                                        <div class="documento"><ul>
                                            <?php while ( $query4->have_posts() ) : $query4->the_post();


                                        $descargadoc = get_field('descarga');

                                        $descargaenlace = get_field('enlace_descarga');


                                        if( !empty($descargadoc) ):

                                        $descarga = $descargadoc;

                                        else:
                                        $descarga = $descargaenlace;

                                        endif
                                    ?>

                                <?php if  (!empty($descargadoc || $descargaenlace)):?>

                                <li><?php the_title();

                                    ?>
                                    <a href="<?php echo $descarga; ?>"target="_blank"><i class="bbva-coronita_download pl10 descarga"></i></a></li>
                                    <?php endif; ?>
                                    <?php

                                    endwhile; ?>
                                   </ul> </div> <?php endif; ?>

                            </ul>
                        </div></div>


                        </div>

                    </div>
                </div>

        </section>
        </div>



        <?php endwhile; ?>
                                <?php endif; ?>


    <div class="back-image-lands">
        <img class="image-rot" src="<?php echo get_template_directory_uri();?>/img/landscape.png" alt="" />
        <p class="aqua text-d-none" style="margin: 0;padding-bottom: 13px;font-size: 18px; font-weight: 700;position: relative;bottom: 6px; text-align: center !important;">Para disfrutar de una mejor experiencia, por favor gira tu teléfono.</p>
    </div>


<?php get_footer();
