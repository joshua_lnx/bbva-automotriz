<?php

/*
* Template Name: Plantilla Esquema/Formatos
*/
 get_header();



?>



<?php
$categoria = get_field('nombre_categoria');
    $args = array(
        'post_type' => 'esquema', // the post type
        'posts_per_page'=>'30',
        'tax_query' => array(
            array(
                'taxonomy' => 'Categorias_esquema', // the custom vocabulary
                'field'    => 'slug',
                'terms'    => array($categoria),      // provide the term slugs
            ),
        ),
    );

 ?>







<div class="display-none-mobil">
<div class="hero-banner2">
    <img src="<?php echo get_the_post_thumbnail_url(); ?>">

</div>

<section>

    <div class="container-fluid bg-primary ">
        <div class="container my-5">
            <div class="row">
                <div class="col-sm-3">
                    <div class="content-text">
                    <img class="my-6" src="<? echo get_field('icono')?>" height="24px" width="24px" style="margin-top: 30px;">

                        <h2 class="thin"> <br><?php echo get_the_title(); ?></h2>


                    </div>
                </div>
                <div class="col-sm-9 white mx-6">
                    <div class="bg-white">
                        <div class="card-body my-3 bbva-cards"></div>

                        <h6 class="card-title mx-4 mb-4 h6 bullet categoria-9 " :after> Documentos</h6>
                        <ul>

                               <?php

                           $query = new WP_Query ( $args);
                            if ( $query->have_posts() ) :
                            ?>
                                 <div class="documento"><ul>
                                    <?php while ( $query->have_posts() ) : $query->the_post();


                                $descargadoc = get_field('descarga');

                                $descargaenlace = get_field('enlace_descarga');


                                 if( !empty($descargadoc) ):

                                $descarga = $descargadoc;

                                else:
                                $descarga = $descargaenlace;

                                endif
                            ?>

                           <?php if  (!empty($descargadoc || $descargaenlace)):?>

                           <li><?php the_title();

                              ?>
                              <a href="<?php echo $descarga; ?>"target="_blank"><i class="bbva-coronita_download pl10 descarga"></i></a></li>
                            <?php endif; ?>
                            <?php

                            endwhile; ?>
                           </ul> </div> <?php endif; ?>
                        </ul>
                    </div>


                </div>



            </div>
        </div>
    </div>

</section>





<?php  if (have_posts()): ?>
<?php   while(have_posts()): the_post(); ?>


</div>



<?php endwhile;?>
<?php endif;?>



 <div class="back-image-lands">
        <img class="image-rot" src="<?php echo get_template_directory_uri();?>/img/landscape.png" alt="" />
        <p class="aqua text-d-none" style="margin: 0;padding-bottom: 13px;font-size: 18px; font-weight: 700;position: relative;bottom: 6px; text-align: center !important;">Para disfrutar de una mejor experiencia, por favor gira tu teléfono.</p>
    </div>

<?php get_footer();