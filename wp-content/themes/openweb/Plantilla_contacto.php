<?php

/**
 * Template Name: Plantilla Contacto
 * Template Post Type: page
 * Template Description: Plantilla para buzon de sugerencias
 *
 * @package OpenWeb
 * @subpackage Coronita
 * @since OpenWeb Coronita 1.0
 */

get_header();
?>

<?php
while (have_posts()) {
    the_post();
}
?>


<div class="container-fluid">
    <div class="container">
        <div class="col-xs-12 col-md-offset-2 col-md-8">
            <div class="form-wrap">
                <br>
                <br>
                <h2 style="text-align:center;"><?php echo the_title_attribute(); ?></h2>
                <!--INICIA FORM-->
                <h6 class="no-margin" style="text-align:center"><?php the_field('descripcion'); ?></h6>
                <br>
                <br>
                <div data-component-bbva="form" data-formid="jy65l5nl1571264013045"></div>

            </div>

        </div>
    
    </div>

</div>


<div class="pt40 pb40"></div>
<?php get_footer(); ?>


<script src="https://d3l7jhiu2gy1zw.cloudfront.net/lib/bbva-component/core.js" data-bbva-project="jy65l5nl" data-bbva-api="https://revision-jy65l5nl.openweb.bbva"></script>