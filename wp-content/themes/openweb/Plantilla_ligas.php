<?php

/*
* Template Name: Plantilla Ligas de Interes
*/
get_header();



?>

<?php
$categoria = get_field('nombre_categoria');
$args = array(
    'post_type' => 'esquema', // the post type
    'tax_query' => array(
        array(
            'taxonomy' => 'Categorias_esquema', // the custom vocabulary
            'field'    => 'slug',
            'terms'    => array($categoria),      // provide the term slugs
        ),
    ),
);

?>
<style>
h6.bullet, h6 .bullet, .h6.bullet, h6.bullet a, .h6.bullet a, h6 .bullet a {
    color: #121212 !important;
    font-family: "BentonSansBBVA Medium", "Helvetica Neue", Arial, Helvetica, sans-serif;
    font-size: 1.5rem !important;
    margin-bottom: 1em !important;
    text-transform: uppercase !important;
    
}

p ::before{
   /* padding-right:20px !important; */
    color: #1973b8 !important;
    content: "-"!important;  
}
   p{
    line-height: .8em !important;
    font-size: 1.5rem !important;
   }
   

}
</style>


<div class="display-none-mobil">
<section>
    <div class="container-fluid bg-primary ">
        <div class="container my-5">
            <div class="row">
                <div class="col-sm-3">
                    <div class="content-text">
                        <img class="my-6" src="<? echo get_field('icono') ?>" height="24px" width="24px" style="margin-top: 30px;">

                        <h2 class="thin"> <br><?php echo get_the_title(); ?></h2>

                    </div>
                </div>
                <div class="col-sm-9 white mx-6">
                    <div class="bg-white">
                        <div class="card-body my-3 bbva-cards">

                        <h6 style="target-new: tab;" class="bullet categoria-9" :after> Enlaces</h6>
                        


                            <?php
                            //mostrar contenido de la pagina
                            while (have_posts()) : the_post(); ?>
                                <!--El the_content() solo trabaja dentro de un WP loop -->
                                <!-- Ahora si, el the_content -->


                                
<base target="_blank">
                      
                            <?php the_content(); ?>
                           
                         
                                
                          
                            
                            
                            <?php
                            endwhile;
                            wp_reset_query();
                            ?>
</div>
                            <div>



                                
                            </div>

                        
                    </div>


                </div>



            </div>
        </div>
    </div>

</section>









</div>



 <div class="back-image-lands">
        <img class="image-rot" src="<?php echo get_template_directory_uri();?>/img/landscape.png" alt="" />
        <p class="aqua text-d-none" style="margin: 0;padding-bottom: 13px;font-size: 18px; font-weight: 700;position: relative;bottom: 6px; text-align: center !important;">Para disfrutar de una mejor experiencia, por favor gira tu teléfono.</p>
    </div>

<?php get_footer();
