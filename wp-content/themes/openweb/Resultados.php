<?php
/**
 * Template Name: Openweb - Plantilla Buscador 
 * Template Post Type: page
 * Template Description: Plantilla para los resultados de la búsqueda en AWS CloudSearch
 *
 */

get_header();

?>

<div class="container-fluid">
    <div class="container">
    <div class="hidden" id="openweb-item">
        <div class="cq-searchpromote-result-item">
            <div class="description">
                <a href="#" class="openweb-url-item">
                    <strong class="openweb-title-item"></strong>
                </a>
                <p class="openweb-excerpt-item"></p>
            </div>
            <div style="clear:both"></div>
        </div>
    </div> 

    <div class="results section">
        <div class="cq-searchpromote-results container">
            <div id="openweb-header-results">
                <h1 class="h1"><?php echo __('Resultados de la búsqueda', 'openweb'); ?></h1>
                <p class="hidden" id="openweb-count"><?php echo __('Mostrando resultados', 'openweb'); ?> <em id="openweb-results-range"></em> <?php echo __('de', 'openweb'); ?> 
                <em id="openweb-results-count"></em> <?php echo __('para', 'openweb'); ?> <strong id="term"></strong></p>


                <div class="row">
                    <div class="col-xs-12">
                        <p class="hidden" id="openweb-non-results"><?php echo __('No se han encontrado resultados para ', 'openweb'); ?></p>
                    </div>
                </div>
            </div>

            <div id="openweb-results"></div>
        </div>
    </div>

    <div class="pagination section">
        <div class="cq-searchpromote-pagination container" id="openweb-pagination"></div>
    </div>
    </div>
</div>
<?php get_footer();?>


<script>

jQuery.get(
           "https://revision-jy65l5nl.openweb.bbva/bbva-components/search/?project=jy65l5nl&q=*&q.parser=lucene", function( data ) {
           console.log(data);});

</script>



<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/base.js?ver=1.0.0"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/app.js?ver=1.0.0"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/assets/bootstrap-select/js/bootstrap-select.min.js?ver=1.0.0"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/search.js"></script>

<script src="https://d3l7jhiu2gy1zw.cloudfront.net/lib/bbva-component/core.js" data-bbva-project="jy65l5nl" data-bbva-api="jy65l5nl.openweb.bbva"></script>



