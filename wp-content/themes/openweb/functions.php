<?php

/**
 * OpenWeb functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package OpenWeb
 * @subpackage Theme
 * @since Theme 1.0
 */
if (version_compare(PHP_VERSION, '5.6', '<')) {
    die(__('El tema Openweb basado en Coronita sólo funciona a partir de las versiones 5.6.x de PHP', 'openweb'));
}

if (version_compare($GLOBALS['wp_version'], '4.8', '<')) {
    die(__('El tema Openweb basado en Coronita sólo funciona a partir de las versiones 4.8 de Wordpress', 'openweb'));
}

define('OPENWEB_THEME_PATH', __DIR__);

require_once OPENWEB_THEME_PATH.'/vendor/autoload.php';

$theme = \OpenWeb\Theme::getInstance();
$theme->init();


add_filter('use_block_editor_for_post_type', '__return_false', 100);


// Custom Post Type

function noticias_register_custom_post_type()
{
    /* Añado las etiquetas que aparecerán en el escritorio de WordPress */
    $labels2 = array(
    'name'               => _x('Noticias', 'post type general name', 'text-domain'),
    'singular_name'      => _x('Noticias', 'post type singular name', 'text-domain'),
    'menu_name'          => _x('Noticias', 'admin menu', 'text-domain'),
    'add_new'            => _x('Añadir nueva', 'Noticia', 'text-domain'),
    'add_new_item'       => __('Añadir nueva Noticia', 'text-domain'),
    'new_item'           => __('Nueva Noticia', 'text-domain'),
    'edit_item'          => __('Editar Noticia', 'text-domain'),
    'view_item'          => __('Ver Noticia', 'text-domain'),
    'all_items'          => __('Todas las Noticias', 'text-domain'),
    'search_items'       => __('Buscar Noticias', 'text-domain'),
    'not_found'          => __('No hay Noticias.', 'text-domain'),
    'not_found_in_trash' => __('No hay Noticias en la papelera.', 'text-domain'),
  );
    // $capabilities_noticias = array(
    //     'edit_post'          => 'edit_noticias',
    //     'edit_posts'         => 'edit_noticias',
    //     'edit_others_posts'  => 'edit_other_noticias',
    //     'publish_posts'      => 'publish_noticias',
    //     'read_post'          => 'read_noticias',
    //     'read_private_posts' => 'read_private_noticias',
    //     'delete_post'        => 'delete_noticias',
    //     'read_posts'         => 'read_noticias',
    // );

    /* comportamiento y funcionalidades del nuevo custom post type */
    $args2 = array(
    'labels'             => $labels2,
    'menu_icon'          => __('dashicons-media-document'),
    'description'        => __('Descripción.', 'text-domain'),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'noticias' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => null,
    'supports'           => array( 'title', 'editor', 'author', 'comments' ),
    'publicly_queryable' => true,
    'capability_type'    => 'post',
    //'capabilities'       => $capabilities_noticias,
  );
    register_post_type('Noticias1', $args2);
}
add_action('init', 'noticias_register_custom_post_type');



function productos_register_custom_post_type()
{
    /* Añado las etiquetas que aparecerán en el escritorio de WordPress */
    $labels2 = array(
    'name'               => _x('Productos', 'post type general name', 'text-domain'),
    'singular_name'      => _x('Productos', 'post type singular name', 'text-domain'),
    'menu_name'          => _x('Productos', 'admin menu', 'text-domain'),
    'add_new'            => _x('Añadir nuevo', 'Producto', 'text-domain'),
    'add_new_item'       => __('Añadir nuevo Producto', 'text-domain'),
    'new_item'           => __('Nuevo Producto', 'text-domain'),
    'edit_item'          => __('Editar Producto', 'text-domain'),
    'view_item'          => __('Ver Producto', 'text-domain'),
    'all_items'          => __('Todas los Productos', 'text-domain'),
    'search_items'       => __('Buscar Productos', 'text-domain'),
    'not_found'          => __('No hay Productos.', 'text-domain'),
    'not_found_in_trash' => __('No hay Productos en la papelera.', 'text-domain'),
  );
    // $capabilities_noticias = array(
    //     'edit_post'          => 'edit_noticias',
    //     'edit_posts'         => 'edit_noticias',
    //     'edit_others_posts'  => 'edit_other_noticias',
    //     'publish_posts'      => 'publish_noticias',
    //     'read_post'          => 'read_noticias',
    //     'read_private_posts' => 'read_private_noticias',
    //     'delete_post'        => 'delete_noticias',
    //     'read_posts'         => 'read_noticias',
    // );

    /* comportamiento y funcionalidades del nuevo custom post type */
    $args2 = array(
    'labels'             => $labels2,
    'menu_icon'          => __('dashicons-products'),
    'description'        => __('Descripción.', 'text-domain'),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'productos' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => null,
    'supports'           => array( 'title', 'editor', 'author', 'comments' ),
    'publicly_queryable' => true,
    'capability_type'    => 'post',
    //'capabilities'       => $capabilities_noticias,
  );
    register_post_type('productos', $args2);
}
add_action('init', 'productos_register_custom_post_type');



function alianzas_register_custom_post_type()
{
    /* Añado las etiquetas que aparecerán en el escritorio de WordPress */
    $labels2 = array(
    'name'               => _x('Alianzas', 'post type general name', 'text-domain'),
    'singular_name'      => _x('Alianzas', 'post type singular name', 'text-domain'),
    'menu_name'          => _x('Alianzas', 'admin menu', 'text-domain'),
    'add_new'            => _x('Añadir nueva', 'Alianza', 'text-domain'),
    'add_new_item'       => __('Añadir nueva', 'text-domain'),
    'new_item'           => __('Nueva Alianza', 'text-domain'),
    'edit_item'          => __('Editar Alianza', 'text-domain'),
    'view_item'          => __('Ver Alianza', 'text-domain'),
    'all_items'          => __('Todas las Alianzas', 'text-domain'),
    'search_items'       => __('Buscar Alianzas', 'text-domain'),
    'not_found'          => __('No hay Alianzas.', 'text-domain'),
    'not_found_in_trash' => __('No hay Alianzas en la papelera.', 'text-domain'),
  );
    // $capabilities_noticias = array(
    //     'edit_post'          => 'edit_noticias',
    //     'edit_posts'         => 'edit_noticias',
    //     'edit_others_posts'  => 'edit_other_noticias',
    //     'publish_posts'      => 'publish_noticias',
    //     'read_post'          => 'read_noticias',
    //     'read_private_posts' => 'read_private_noticias',
    //     'delete_post'        => 'delete_noticias',
    //     'read_posts'         => 'read_noticias',
    // );

    /* comportamiento y funcionalidades del nuevo custom post type */
    $args2 = array(
    'labels'             => $labels2,
    'menu_icon'          => __('dashicons-groups'),
    'description'        => __('Descripción.', 'text-domain'),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'alianzas' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => null,
    'supports'           => array( 'title', 'editor', 'author', 'comments' ),
    'publicly_queryable' => true,
    'capability_type'    => 'post',
    //'capabilities'       => $capabilities_noticias,
  );
    register_post_type('alianzas', $args2);
}
add_action('init', 'alianzas_register_custom_post_type');