<?php
    /**
     *Template Name: Plantilla de Inicio
     **/
    require_once __DIR__.'/vendor/autoload.php';
    $theme = \OpenWeb\Theme::getInstance();
    $articles = [];
    get_header();
?>

    <link href="<?php echo get_template_directory_uri();?>/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet"
    id="bootstrap-css">
    <?php
        $post_id = 41;

        $Imagen1 = get_field('imagen_1', $post_id);
        $Imagen2 = get_field('imagen_2', $post_id);
        $Imagen3 = get_field('imagen_3', $post_id);

        $Titulo1 = get_field('titulo_1', $post_id);
        $Titulo2 = get_field('titulo_2', $post_id);
        $Titulo3 = get_field('titulo_3', $post_id);

        $imagen1 = $Imagen1;
        $imagen2 = $Imagen2;
        $imagen3 = $Imagen3;

    ?>
<div class="display-none-mobil">
    <?php if  (!empty($imagen1) || ($imagen2) || ($imagen3)):?>
    <div class="container-fluid container-banner">
        <!-- Carousel -->
        <div id="carousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php  if( !empty($imagen1) ): ?>
                <li data-target="#carousel" data-slide-to="0" class="active"></li>
                <?php endif; ?>

                <?php  if( !empty($imagen2) ): ?>
                <?php if (empty($imagen1)) {
                            $clase1="active";
                            $orden="0";
                        }
                        else{
                            $clase1="";
                            $orden="1";

                        }
                ?>

                <li data-target="#carousel" data-slide-to="<?php echo $orden; ?>" class="<?php echo $clase1; ?>"></li>
                <?php endif; ?>
                <?php  if( !empty($imagen3) ): ?>
                <?php if (empty($imagen1 || $imagen2)) {
                            $clase1="active";
                            $orden="0";
                        }
                        elseif (empty($imagen1) or empty($imagen2)){
                            $clase1="";
                            $orden="1";

                        }
                        else {
                            $clase1="";
                            $orden="2";

                        }?>
                <li data-target="#carousel" data-slide-to="<?php echo $orden; ?>" class="<?php echo $clase1; ?>"></li>
                <?php endif; ?>
            </ol>
            <!-- Wrapper for slides -->

            <div class="carousel-inner">
                <?php  if( !empty($imagen1) ): ?>
                <div class="item active">
                    <img src="<?php echo $Imagen1; ?>" alt="Slide1">
                    <!-- Static Header -->
                    <div class="header-text">

                        <div class="col-xs-10 col-sm-7 col-md-6 col-lg-4 card-hero-home">
                            <div class="card-text">
                                <?php echo $Titulo1; ?>
                            </div>
                        </div>

                    </div><!-- /header-text -->
                </div><?php endif; ?>
                <?php  if( !empty($imagen2) ): ?>
                <?php  if (empty($imagen1 || $imagen3))
                        {
                            $clase= "active";
                            }
                            elseif(empty($imagen1) and !empty($imagen3))
                            {
                                $clase="active";

                            }
                            else {
                            $clase="";
                            }?>

                <div class="item <?php echo $clase ?>">
                    <img src="<?php echo $Imagen2; ?>" alt="Slide2">
                    <!-- Static Header -->
                    <div class="header-text">

                        <div class="col-xs-10 col-sm-7 col-md-6 col-lg-4 card-hero-home">
                            <div class="card-text">
                                <?php echo $Titulo2; ?>
                            </div>
                        </div>

                    </div><!-- /header-text -->
                </div><?php endif; ?>
                <?php  if( !empty($imagen3) ):  ?>
                <?php  if (empty($imagen1 || $imagen2))
                                {
                                $clase2= "active";
                                }
                                else
                                {
                                    $clase2="";

                                }?>

                <div class="item <?php echo $clase2 ?>">
                    <img src="<?php echo $Imagen3; ?>" alt="Slide3">
                    <!-- Static Header -->
                    <div class="header-text">

                        <div class="col-xs-10 col-sm-7 col-md-6 col-lg-4 card-hero-home">
                            <div class="card-text">
                                <?php echo $Titulo3; ?>
                            </div>
                        </div>

                    </div><!-- /header-text -->
                </div><?php endif; ?>
            </div>
            <!-- Controls -->
            <a class="" href="#carousel" data-slide="prev">
                <span class="glyphicon"></span>
            </a>
            <a class="" href="#carousel" data-slide="next">
                <span class="glyphicon"></span>
            </a>
        </div><!-- /carousel -->
    </div>
    <?php endif; ?>

    <section class="container-fluid bg-grey100">
        <div>
            <?php $theme->renderView('loop/structure', $articles); ?>
        </div>
    </section>
</div>

<div class="back-image-lands">
    <img class="image-rot" src="<?php echo get_template_directory_uri();?>/img/landscape.png" alt="" />
    <p class="aqua text-d-none"
        style="margin: 0;padding-bottom: 13px;font-size: 18px; font-weight: 700;position: relative;bottom: 6px; text-align: center !important;">
        Para disfrutar de una mejor experiencia, por favor gira tu teléfono.</p>
</div>


<?php get_footer();