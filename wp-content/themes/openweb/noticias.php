<?php
/**
    *Template Name: Plantilla de Noticias
**/
?>

<?php get_header();?>

<?php

$args = array(
    'post_type' => 'Noticias1',
    'showposts'  => '15'
);
$noticias = new WP_Query($args);
?>

    <div class="display-none-mobil">
        <div class="container">
            <h1 class="text-center thin pt40 pb40">Últimas noticias</h1>
            <div class="row my-posts3">
                <?php while ($noticias->have_posts()): $noticias->the_post();?>
                <div class="col-12 col-sm-6 col-md-4 mb-3">
                    <div class="cards-image-noticias">
                        <?php
                            $enlace_imagen = get_field('link_image');

                            $imagen = get_field('imagen');


                             if (!empty($enlace_imagen)) {
                                 $url_imagen = $enlace_imagen;
                             } else {
                                $url_imagen = $imagen;
                            }
                        ?>
                        <img src="<?php echo $url_imagen; ?>"/>
                    </div>
                    <div class="card-text-sec-noticias pt20">
                        <?php
                            $titulo = get_the_title(); //Obtener el titulo
                            echo '<h4>'. substr($titulo, 0, 80).'</h4>'; //Limitar a 80 caracteres
                        ?>
                        <?php $description = get_field('description');?>
                        <p> <?php echo $description; ?></p>
                        <?php
                        $link = get_field('link'); ?>

                        <a href="<?php echo $link; ?>" target="_blank"><i class="bbva-coronita_visual blue-medium" style="margin:8px !important;"></i>Ver más</a>

                    </div>
                </div>
                <?php endwhile; ?>
            </div>
            <br />
        </div>
    </div>
    <div class="back-image-lands">
        <img class="image-rot" src="<?php echo get_template_directory_uri();?>/img/landscape.png" alt="" />
        <p class="aqua text-d-none" style="margin: 0;padding-bottom: 13px;font-size: 18px; font-weight: 700;position: relative;bottom: 6px; text-align: center !important;">Para disfrutar de una mejor experiencia, por favor gira tu teléfono.</p>
    </div>
<?php get_footer();?>
