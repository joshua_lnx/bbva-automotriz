<?php
/*
Template Name: Plantilla Mercado y Competencia
*/
?>

<?php get_header(); ?>

<body style="background: #f4f4f4;">
    <div class="display-none-mobil">
        <h1 class="thin text-center pt40"><?php the_title(); ?></h1>
        <div class="container-fluid">
            <div class="container">
                <div class="row row_mercado" style="display: flex; justify-content: center;">
                    <div class="col-sm-4">
                        <div class="card">
                            <?php if(have_rows('card_1')):
                                while(have_rows('card_1') ): the_row();
                                    // vars
                                    $title_1 = get_sub_field('title_1');
                                    $image_1 = get_sub_field('image_1');
                                    $url = get_permalink(347);
                                    $descripcion1 = get_sub_field('descripcion');

                                    ?>
                                    <div id="card_1">
                                        <a href="<?php echo $url;?>">
                                        <img class="cards-image" src="<?php echo $image_1['url']; ?>" alt="<?php echo $image_1['alt']; ?>" />
                                    </div>
                                    <?php if($title_1): ?>
                                    <div class="card-text-products pt10">
                                        <h4><?php echo $title_1; ?></h4>
                                        <p><?php echo $descripcion1; ?></p>
                                    </div></a>
                                    <?php endif; ?>
                                    <style type="text/css">
                                        #card_1 {
                                            background: <?php the_sub_field('background_1');
                                            ?>;
                                            max-height: 280px;
                                        }
                                    </style>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="card">
                            <?php if(have_rows('card_2')):
                                while(have_rows('card_2')): the_row();
                                    // vars
                                    $titulo_2 = get_sub_field('titulo_2');
                                    $imagen_2 = get_sub_field('imagen_2');
                                    $url = get_permalink(349);
                                    $descripcion1 = get_sub_field('descripcion');

                                    ?>
                                    <div id="card_dos">
                                    <a href="<?php echo $url;?>">
                                        <img class="cards-image" src="<?php echo $imagen_2['url']; ?>" alt="<?php echo $imagen_2['alt']; ?>" />
                                    </div>
                                    <?php if($titulo_2): ?>
                                    <div class="card-text-products pt10">
                                        <h4><?php echo $titulo_2; ?></h4>
                                        <p><?php echo $descripcion1; ?></p>
                                        </div></a>
                                    <?php endif; ?>
                                    <style type="text/css">
                                        #card_dos {
                                            background: <?php the_sub_field('color_de_fondo_2');
                                            ?>;
                                            max-height: 280px;
                                        }
                                    </style>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php $card_tres = get_sub_field('card_tres'); ?>
                    <?php if(!empty($card_tres)):?>
                    <div class="col-sm-4">
                        <div class="card">
                            <?php if(have_rows('card_tres') ):
                                while(have_rows('card_tres') ): the_row();
                                    // vars
                                    $titulo_3 = get_sub_field('titulo_3');
                                    $card_tres = get_field('card_tres');
                                    $imagen_3 = get_sub_field('imagen_3');

                                    ?>
                                    <div id="card_tres" class="container-flex">
                                        <img class="cards-image" src="<?php echo $imagen_3['url']; ?>" alt="<?php echo $imagen_3['alt']; ?>" />
                                    </div>
                                    <?php if($titulo_3): ?>
                                    <div class="card-text-products pt30">
                                        <h4><?php echo $titulo_3; ?></h4>
                                    </div>
                                    <?php endif; ?>
                                    <style type="text/css">
                                        #card_tres {
                                            background: <?php the_sub_field('color_de_fondo_3');
                                            ?>;
                                            max-height: 280px;
                                        }
                                    </style>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="back-image-lands">
        <img class="image-rot" src="<?php echo get_template_directory_uri();?>/img/landscape.png" alt="" />
        <p class="aqua text-d-none" style="margin: 0;padding-bottom: 13px;font-size: 18px; font-weight: 700;position: relative;bottom: 6px; text-align: center !important;">Para disfrutar de una mejor experiencia, por favor gira tu teléfono.</p>
    </div>
<?php get_footer(); ?>