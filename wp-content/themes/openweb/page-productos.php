<?php
/*
Template Name: Plantilla Productos Principal
*/
?>
<?php get_header(); ?>


<body style="background: #f4f4f4;">

    <div class="display-none-mobil">
        <h1 class="thin text-center pt40">
        <?php the_title(); ?>
    </h1>
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                <div class="card">
                    <?php if( have_rows('card_1')):
                        while( have_rows ('card_1') ): the_row();
                        // vars
                        $titulo_1 = get_sub_field('titulo_1');
                        $card_1 = get_field('card_1');
                        $imagen = get_sub_field('imagen');
                        $url = get_permalink(411);
                        $descripcion1 = get_sub_field('descripcion');
                        ?>
                    <a href="<?php echo "$url"; ?>">
                    <div id="card_1" class="container-flex">
                        <img class="cards-image" src="<?php echo $imagen['url']; ?>"
                            alt="<?php echo $imagen['alt']; ?>" />
                    </div>
                    <div class="card-text-products pt10">
                        <h4>
                        <?php echo $titulo_1; ?>
                        </h4>
                        <p>
                        <?php echo $descripcion1; ?>
                        </p>
                        </a>
                    </div>
                    <style type="text/css">
                    #card_1 {
                        background: <?php the_sub_field('color_de_fondo_1');
                        ?>;
                        height: 280px;
                    }
                    </style>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
                </div>
                <div class="col-sm-4">
                <div class="card">
                    <?php if( have_rows('card_2') ):
                        while( have_rows('card_2') ): the_row();
                        // vars
                        $titulo_2 = get_sub_field('titulo_2');
                        $card_2 = get_field('card_2');
                        $imagen_2 = get_sub_field('imagen_2');
                        $url = get_permalink(383);
                        $descripcion1 = get_sub_field('descripcion');
                        ?>
                    <a href="<?php echo "$url"; ?>">
                    <div id="card_2" class="container-flex">
                        <img class="cards-image" src="<?php echo $imagen_2['url']; ?>"
                            alt="<?php echo $imagen_2['alt']; ?>" />
                    </div>
                    <div class="card-text-products pt10">
                        <h4>
                        <?php echo $titulo_2; ?>
                        </h4>
                        <p>
                        <?php echo $descripcion1; ?>
                        </p>
                        </a>
                    </div>
                    <style type="text/css">
                    #card_2 {
                        background: <?php the_sub_field('color_de_fondo_2');
                        ?>;
                        height: 280px;
                    }
                    </style>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
                </div>
                <div class="col-sm-4">
                <div class="card">
                    <?php if( have_rows('card_3') ):
                        while( have_rows('card_3') ): the_row();
                        // vars
                        $titulo_3 = get_sub_field('titulo_3');
                        $card_3 = get_field('card_3');
                        $imagen_3 = get_sub_field('imagen_3');
                        $url = get_permalink(413);
                        $descripcion1 = get_sub_field('descripcion');
                        ?>
                    <a href="<?php echo "$url"; ?>">
                    <div id="card_3" class="container-flex">
                        <img class="cards-image" src="<?php echo $imagen_3['url']; ?>"
                            alt="<?php echo $imagen_3['alt']; ?>" />
                    </div>
                    <div class="card-text-products pt10">
                        <h4>
                        <?php echo $titulo_3; ?>
                        </h4>
                        <p>
                        <?php echo $descripcion1; ?>
                        </p>
                        </a>
                    </div>
                    <style type="text/css">
                    #card_3 {
                        background: <?php the_sub_field('color_de_fondo_3');
                        ?>;
                        height: 280px;
                    }
                    </style>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
                </div>
            </div>
            <br>
            <br>
            <!-- Second Row -->
            <div class="row">
                <div class="col-sm-4">
                <div class="card">
                    <?php if( have_rows('card_4') ):
                        while( have_rows('card_4') ): the_row();
                        // vars
                        $titulo_4 = get_sub_field('titulo_4');
                        $card_4 = get_field('card_4');
                        $imagen_4 = get_sub_field('imagen_4');
                        $url = get_permalink(415);
                        $descripcion1 = get_sub_field('descripcion');
                        ?>
                    <a href="<?php echo "$url"; ?>">
                    <div id="card_4" class="container-flex">
                        <img class="cards-image" src="<?php echo $imagen_4['url']; ?>"
                            alt="<?php echo $imagen_4['alt']; ?>" />
                    </div>
                    <div class="card-text-products pt10">
                        <h4>
                        <?php echo $titulo_4; ?>
                        </h4>
                        <p>
                        <?php echo $descripcion1; ?>
                        </p>
                        </a>
                    </div>
                    <style type="text/css">
                    #card_4 {
                        background: <?php the_sub_field('color_de_fondo_4');
                        ?>;
                        height: 280px;
                    }
                    </style>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
                </div>
                <div class="col-sm-4">
                <div class="card">
                    <?php if( have_rows('card_5') ):
                        while( have_rows('card_5') ): the_row();
                        // vars
                        $titulo_5 = get_sub_field('titulo_5');
                        $card_5 = get_field('card_5');
                        $imagen_5 = get_sub_field('imagen_5');
                        $url = get_permalink(417);
                        $descripcion1 = get_sub_field('descripcion');
                        ?>
                    <a href="<?php echo "$url"; ?>">
                    <div id="card_5" class="container-flex">
                        <img class="cards-image" src="<?php echo $imagen_5['url']; ?>"
                            alt="<?php echo $imagen_5['alt']; ?>" />
                    </div>
                    <div class="card-text-products pt10">
                        <h4>
                        <?php echo $titulo_5; ?>
                        </h4>
                        <p>
                        <?php echo $descripcion1; ?>
                        </p>
                        </a>
                    </div>
                    <style type="text/css">
                    #card_5 {
                        background: <?php the_sub_field('color_de_fondo_5');
                        ?>;
                        height: 280px;
                    }
                    </style>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
                </div>
                <div class="col-sm-4">
                <div class="card">
                    <?php if( have_rows('card_6') ):
                        while( have_rows('card_6') ): the_row();
                        // vars
                        $titulo_6 = get_sub_field('titulo_6');
                        $card_6 = get_field('card_6');
                        $imagen_6 = get_sub_field('imagen_6');
                        $url6 = get_permalink(407);
                        $descripcion1 = get_sub_field('descripcion');
                        ?>
                    <a href="<?php echo "$url6"; ?>">
                    <div id="card_6" class="container-flex">
                        <img class="cards-image" src="<?php echo $imagen_6['url']; ?>"
                            alt="<?php echo $imagen_6['alt']; ?>" />
                    </div>
                    <div class="card-text-products pt10">
                        <h4>
                        <?php echo $titulo_6; ?>
                        </h4>
                        <p>
                        <?php echo $descripcion1; ?>
                        </p>
                        </a>
                    </div>
                    <style type="text/css">
                    #card_6 {
                        background: <?php the_sub_field('color_de_fondo_6');
                        ?>;
                        height: 280px;
                    }
                    </style>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
                </div>
            </div>
            <!-- Second Row -->
            </div>
        </div>
    </div>
    <div class="back-image-lands">
        <img class="image-rot" src="<?php echo get_template_directory_uri();?>/img/landscape.png" alt="" />
        <p class="aqua text-d-none" style="margin: 0;padding-bottom: 13px;font-size: 18px; font-weight: 700;position: relative;bottom: 6px; text-align: center !important;">Para disfrutar de una mejor experiencia, por favor gira tu teléfono.</p>
    </div>


  <?php get_footer(); ?>
