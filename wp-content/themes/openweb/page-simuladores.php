<?php
/*
Template Name: Plantilla Simuladores
*/
?>

<?php get_header(); ?>

<body style="background: #f4f4f4;">
<h1 class="thin text-center pt40"><?php
        $titulo = get_field('titulo');
        echo $titulo;
        ?>
</h1>
<div class="container-fluid">
    <div class="container">
        <div class="row" style="display: flex; justify-content: center;">
            <div class="col-sm-4">
                <div class="card pb40">
                    <?php if(have_rows('card_1')):
                        while(have_rows('card_1') ): the_row();
                            $title = get_sub_field('title');
                            $image = get_sub_field('image');
                            $description = get_sub_field('description');
                            ?>
                            <div id="card_1">
                                <img class="cards-image" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                            </div>
                            <?php if($title): ?>
                            <div class="card-text-products pt30">
                                <h4><?php echo $title; ?></h4>
                                <p class="disclosure"><?php echo $description; ?></p>
                            </div>
                            <?php endif; ?>
                            <style type="text/css">
                                #card_1 {
                                    background: <?php the_sub_field('background');
                                    ?>;
                                    max-height: 280px;
                                }
                            </style>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card pb40">
                    <?php if(have_rows('card_2')):
                        while(have_rows('card_2')): the_row();
                            // vars
                            $title = get_sub_field('title');
                            $image = get_sub_field('image');
                            $description = get_sub_field('description');
                            ?>
                            <div id="card_dos">
                                <img class="cards-image" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                            </div>
                            <?php if($title): ?>
                            <div class="card-text-products pt30">
                                <h4><?php echo $title; ?></h4>
                                <p class="disclosure"><?php echo $description; ?></p>
                            </div>
                            <?php endif; ?>
                            <style type="text/css">
                                #card_dos {
                                    background: <?php the_sub_field('background');
                                    ?>;
                                    max-height: 280px;
                                }
                            </style>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
            <?php $card_3 = get_field('card_3'); ?>
            <?php if(!empty($card_3)):?>
                <div class="col-sm-4">
                    <div class="card pb40">
                        <?php if(have_rows('card_3') ):
                            while(have_rows('card_3') ): the_row();
                                $title = get_sub_field('title');
                                $image = get_sub_field('image');
                                $description = get_sub_field('description');
                                ?>
                                <div id="card_tres" class="container-flex">
                                    <img class="cards-image" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                                </div>
                                <?php if($title): ?>
                                <div class="card-text-products pt30">
                                    <h4><?php echo $title; ?></h4>
                                    <p class="disclosure"><?php echo $description; ?></p>
                                </div>
                                <?php endif; ?>
                                <style type="text/css">
                                    #card_tres {
                                        background: <?php the_sub_field('background');
                                        ?>;
                                        max-height: 280px;
                                    }
                                </style>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <div class="row" style="display: flex; justify-content: center;">
            <?php
            $card_4 = get_field('card_4');
            if(!empty($card_4)):?>
                <div class="col-sm-4">
                    <div class="card pb40">
                        <?php if(have_rows('card_4') ):
                            while(have_rows('card_4') ): the_row();
                                $title = get_sub_field('title');
                                $image = get_sub_field('image');
                                $description = get_sub_field('description');
                                ?>
                                <div id="card_4" class="container-flex">
                                    <img class="cards-image" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                                </div>
                                <?php if($title): ?>
                                <div class="card-text-products pt30">
                                    <h4><?php echo $title; ?></h4>
                                    <p class="disclosure"><?php echo $description; ?></p>
                                </div>
                                <?php endif; ?>
                                <style type="text/css">
                                    #card_4 {
                                        background: <?php the_sub_field('background');
                                        ?>;
                                        max-height: 280px;
                                    }
                                </style>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php
            $card_5 = get_field('card_5');
            if(!empty($card_5)):?>
                <div class="col-sm-4">
                    <div class="card pb40">
                        <?php if(have_rows('card_5') ):
                            while(have_rows('card_5') ): the_row();
                                $title = get_sub_field('title');
                                $image = get_sub_field('image');
                                $description = get_sub_field('description');
                                ?>
                                <div id="card_5" class="container-flex">
                                    <img class="cards-image" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                                </div>
                                <?php if($title): ?>
                                <div class="card-text-products pt30">
                                    <h4><?php echo $title; ?></h4>
                                    <p class="disclosure"><?php echo $description; ?></p>
                                </div>
                                <?php endif; ?>
                                <style type="text/css">
                                    #card_5 {
                                        background: <?php the_sub_field('background');
                                        ?>;
                                        max-height: 280px;
                                    }
                                </style>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php
            $card_6 = get_field('card_6');
            if(!empty($card_6)):?>
                <div class="col-sm-4">
                    <div class="card pb40">
                        <?php if(have_rows('card_6') ):
                            while(have_rows('card_6') ): the_row();
                                $title = get_sub_field('title');
                                $image = get_sub_field('image');
                                $description = get_sub_field('description');
                                ?>
                                <div id="card_6" class="container-flex">
                                    <img class="cards-image" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                                </div>
                                <?php if($title): ?>
                                <div class="card-text-products pt30">
                                    <h4><?php echo $title; ?></h4>
                                    <p class="disclosure"><?php echo $description; ?></p>
                                </div>
                                <?php endif; ?>
                                <style type="text/css">
                                    #card_6 {
                                        background: <?php the_sub_field('background');
                                        ?>;
                                        max-height: 280px;
                                    }
                                </style>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <div class="row">
            <?php
            $card_7 = get_field('card_7');
            if(!empty($card_7)):?>
                <div class="col-sm-4">
                    <div class="card pb40">
                        <?php if(have_rows('card_7') ):
                            while(have_rows('card_7') ): the_row();
                                $title = get_sub_field('title');
                                $image = get_sub_field('image_7');
                                $description = get_sub_field('description');
                                ?>
                                <div id="card_7" class="container-flex">
                                    <img class="cards-image" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                                </div>
                                <?php if($title): ?>
                                <div class="card-text-products pt30">
                                    <h4><?php echo $title; ?></h4>
                                    <p class="disclosure"><?php echo $description; ?></p>
                                </div>
                                <?php endif; ?>
                                <style type="text/css">
                                    #card_7 {
                                        background: <?php the_sub_field('background');
                                        ?>;
                                        max-height: 280px;
                                    }
                                </style>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php
            $card_8 = get_field('card_8');
            if(!empty($card_8)):?>
                <div class="col-sm-4">
                    <div class="card pb40">
                        <?php if(have_rows('card_8') ):
                            while(have_rows('card_8') ): the_row();
                                $title = get_sub_field('title');
                                $image = get_sub_field('image');
                                $description = get_sub_field('description');
                                ?>
                                <div id="card_8" class="container-flex">
                                    <img class="cards-image" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                                </div>
                                <?php if($title): ?>
                                <div class="card-text-products pt30">
                                    <h4><?php echo $title; ?></h4>
                                    <p class="disclosure"><?php echo $description; ?></p>
                                </div>
                                <?php endif; ?>
                                <style type="text/css">
                                    #card_8 {
                                        background: <?php the_sub_field('background');
                                        ?>;
                                        max-height: 280px;
                                    }
                                </style>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>