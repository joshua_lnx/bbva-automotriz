<?php
/**
    *Template Name: Plantilla de Comunicados
**/
?>

<?php get_header();?>

<?php

$args = array(
    'post_type' => 'post',
    'showposts'  => '15'
);
$comunicados = new WP_Query($args);
?>

    <div class="display-none-mobil">
        <div class="container">
            <?php
            echo '<h1 class="text-center thin pt40 pb40">' . get_the_title() . '</h1>';
            ?>
            <div class="row my-posts3">
                <?php while ($comunicados->have_posts() ): $comunicados->the_post();?>
                <div class="col-12 col-sm-6 col-md-4 mb-3">
                    <?php
                        if( has_post_thumbnail()) {
                            the_post_thumbnail('post-thumbnails', array(
                                'class' => 'cards-image-comunicados'
                            ));
                        }else {
                            the_post_thumbnail(' ');
                        }
                    ?>
                    <div class="card-text-sec-noticias pt20">

                        <?php
                            $titulo = get_the_title(); //Obtener el titulo
                            echo '<h4>'. substr($titulo, 0, 70).'</h4>'; //Limitar a 80 caracteres
                        ?>

                        <div class="caja1">
                        <?php
                            $excerpt = get_the_excerpt();
                            echo '<p>' . substr($excerpt, 0, 130). '</p>';
                        ?>
                        </div>
                        <a href="<?php permalink_link(); ?>"><i class="bbva-coronita_visual blue-medium" style="margin:8px !important;"></i>Ver más</a>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
            <br />
        </div>

    </div>
    <div class="back-image-lands">
        <img class="image-rot" src="<?php echo get_template_directory_uri();?>/img/landscape.png" alt="" />
        <p class="aqua text-d-none" style="margin: 0;padding-bottom: 13px;font-size: 18px; font-weight: 700;position: relative;bottom: 6px; text-align: center !important;">Para disfrutar de una mejor experiencia, por favor gira tu teléfono.</p>
    </div>

<?php get_footer();?>