<?php
/*
* Template Name: Plantilla Directorio
*/

?>

<?php get_header(); ?>
<style>


.wpDataTablesWrapper table.wpDataTable>tbody>tr>th, .wpDataTablesWrapper table.wpDataTable>tbody>tr>td{

    text-align: -webkit-center !important;

}


.col-sm-1, .col-sm-1, .col-md-1, .col-md-2, .col-sm-2{
margin-right: 4px !important;

}
.container-fluid {
    padding-top: 2em;
    padding-bottom: 0px!important;
}
.btn{
  padding: 1.5rem !important;
}
</style>
  <div class="display-none-mobil">
    <div class="container-fluid">
      <div class="container">
        <div class="row">
          <center><h1 class="thin pt40"> Directorio hipotecario <strong>BBVA</strong></h1></center>
              <div class="container-fluid">
                  <div class="active"><button class="btn btn-tam col-sm-1 col-xs-12" data-toggle="tab" href="#home"><?php the_field('boton_1'); ?></button></div>
                  <div class=""><button class="btn btn-tam col-sm-1 col-xs-12" type="button" data-toggle="tab" href="#menu1"><?php the_field('boton_2'); ?></button></div>
                  <div class=""><button class="btn btn-tam col-sm-2 col-xs-12" data-toggle="tab" href="#menu2"><?php the_field('boton_3'); ?></button></div>
                  <div class=""><button class="btn btn-tam col-sm-2 col-xs-12"data-toggle="tab" href="#menu3"><?php the_field('boton_4'); ?></a></div>
                  <div class=""><button class="btn btn-tam col-sm-2 col-xs-12" data-toggle="tab" href="#menu4"><?php the_field('boton_5'); ?></button></div>
                  <div class=""><button class="btn btn-tam col-sm-1 col-xs-12" data-toggle="tab" href="#menu5"><?php the_field('boton_6'); ?></button></div>
                  <div class=""><button class="btn btn-tam col-sm-2 col-xs-12" data-toggle="tab" href="#menu6"><?php the_field('boton_7'); ?></button></div>
                  <div class=""><button class="btn btn-tam col-sm-1 col-xs-12" data-toggle="tab" href="#menu7"><?php the_field('boton_8'); ?></button></div>
              </div>

              <div class="tab-content">
                <div id="home" class="tab-pane fade in active">

                  <?php echo do_shortcode('[wpdatatable id=6]'); ?>

                </div>
                <div id="menu1" class="tab-pane fade">
                  <?php echo do_shortcode('[wpdatatable id=7]'); ?>
                </div>
                <div id="menu2" class="tab-pane fade">
                  <?php echo do_shortcode('[wpdatatable id=8]'); ?>
                </div>
                <div id="menu3" class="tab-pane fade">
                  <?php echo do_shortcode('[wpdatatable id=9]'); ?>
                </div>
                <div id="menu4" class="tab-pane fade">
                  <?php echo do_shortcode('[wpdatatable id=10]'); ?>
                </div>
                <div id="menu5" class="tab-pane fade">
                  <?php echo do_shortcode('[wpdatatable id=11]'); ?>
                </div>
                <div id="menu6" class="tab-pane fade">
                  <?php echo do_shortcode('[wpdatatable id=16]'); ?>
                </div>
                <div id="menu7" class="tab-pane fade">
                  <?php echo do_shortcode('[wpdatatable id=24]'); ?>
                </div>
            </div>
          </div>  
        </div>
      </div>
    </div>

  <div class="back-image-lands">
        <img class="image-rot" src="<?php echo get_template_directory_uri();?>/img/landscape.png" alt="" />
        <p class="aqua text-d-none" style="margin: 0;padding-bottom: 13px;font-size: 18px; font-weight: 700;position: relative;bottom: 6px; text-align: center !important;">Para disfrutar de una mejor experiencia, por favor gira tu teléfono.</p>
  </div>



  <?php get_footer();?>
    