<?php

// Template Name: Plantilla Mapa del sitio

get_header();


$post_type_products = new Wp_Query(array(
    'post_type'   => 'page',
    'post_parent' => '53',
));


$post_type_mercado = new Wp_Query(array(
    'post_type'   => 'page',
    'post_parent' => '131',
));
?>

    <div class="display-none-mobil">
        <div class="hero-banner2">
            <img src="<?php echo get_the_post_thumbnail_url (); ?>">
        </div>
        <br /> <br />
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <h3>Productos</h3>
                    <?php if ( $post_type_products->have_posts() ): ?>
                        <?php while ($post_type_products->have_posts() ): $post_type_products->the_post();?>
                        <ul>
                            <li>
                                <a href="<?php the_permalink();?>"><?php the_title(); ?></a>
                            </li>
                        </ul>
                        <?php endwhile;  ?>
                    <?php endif; ?>
                </div>
                <div class="col-sm-4">
                    <h3>Mercado y competencia</h3>
                    <?php if ( $post_type_mercado->have_posts() ): ?>
                        <?php while ($post_type_mercado->have_posts() ): $post_type_mercado->the_post();?>
                        <ul>
                            <li>
                                <a href="<?php the_permalink();?>"><?php the_title(); ?></a>
                            </li>
                        </ul>
                        <?php endwhile;  ?>
                    <?php endif;?>
                </div>
                <div class="col-sm-4">
                    <?php $post_id = 214;?>
                    <h3>Estructura</h3>
                    <ul>
                        <a href="<?php the_permalink($post_id); ?>">
                            <li>Estructura</li>
                        </a>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <?php $post_id = 675;?>
                    <h3>Reportes</h3>
                    <ul>
                        <a href="<?php the_permalink($post_id); ?>">
                            <li>Reportes</li>
                        </a>
                    </ul>
                </div>
                <div class="col-sm-4">
                    <?php $post_id = 679;?>
                    <h3>Negocio promotor</h3>
                    <ul>
                        <a href="<?php the_permalink($post_id); ?>">
                            <li>Negocio promotor</li>
                        </a>
                    </ul>
                </div>
                <div class="col-sm-4">
                    <?php $post_id = 329;?>
                    <h3>Directorio</h3>
                    <ul>
                        <a href="<?php the_permalink($post_id); ?>">
                            <li>Directorio</li>
                        </a>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <?php $post_id = 493;?>
                    <h3>Noticias</h3>
                    <ul>
                        <a href="<?php the_permalink($post_id); ?>">
                            <li>Noticias</li>
                        </a>
                    </ul>
                </div>
                <div class="col-sm-4">
                    <?php $post_id = 698;?>
                    <h3>Comunicados</h3>
                    <ul>
                        <a href="<?php the_permalink($post_id); ?>">
                            <li>Comunicados</li>
                        </a>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="back-image-lands">
        <img class="image-rot" src="<?php echo get_template_directory_uri();?>/img/landscape.png" alt="" />
        <p class="aqua text-d-none" style="margin: 0;padding-bottom: 13px;font-size: 18px; font-weight: 700;position: relative;bottom: 6px; text-align: center !important;">Para disfrutar de una mejor experiencia, por favor gira tu teléfono.</p>
    </div>

<?php get_footer();