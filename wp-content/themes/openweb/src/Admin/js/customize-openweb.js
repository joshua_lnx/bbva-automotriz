'use strict';

(function($) {
    var customizeSearch = '#_customize-input-openweb_json_search';

    var customizeLazy = '#_customize-input-openweb_json_lazy_load';

    var showSpinner = function($e) {
        $e.after($('<span>').attr('id', 'openweb-spinner'));
    };

    var removeSpinner = function() {
        $('#openweb-spinner').remove();
    };

    var regenerate = function(element, action) {
        var txt = element.attr('data-confirm');

        if (confirm(txt)) {
            element.attr('disabled', true);

            var data = {
                'action': action,
                'regenerate': element.attr('data-nonce')
            };

            showSpinner(element);

            $.post(ajaxurl, data, function(d) {
                removeSpinner();
                element.after($('<span>').attr('class', 'openweb-total-regenerate').html(d.count));
                element.attr('disabled', false);
                setTimeout(function() {
                    $('.openweb-total-regenerate').remove();
                }, 1500);
            }, 'json');
        }
    };

    $(document).on('click', customizeSearch, function() {
        regenerate($(this), 'regenerate-search');
    });

    $(document).on('click', customizeLazy, function() {
        regenerate($(this), 'regenerate-lazy-load');
    });
})(jQuery);
