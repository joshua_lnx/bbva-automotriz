<!-- solo visible en desktop-->

<div class="display-none-mobil">
    <div class="container-fluid bg-white cont-mn-rapido text-center">
        <div class="row menu_rapido">
            <?php if (have_rows('title_menu')) :
                while (have_rows('title_menu')) : the_row();
                    $title_1 = get_sub_field('title_1');
                    $title_2 = get_sub_field('title_2');
                    $title_3 = get_sub_field('title_3');
                    $title_4 = get_sub_field('title_4');
                    $title_5 = get_sub_field('title_5');

                    $select_link_1 = get_sub_field('select-page_1');
                    $select_link_2 = get_sub_field('select-page_2');
                    $select_link_3 = get_sub_field('select-page_3');
                    $select_link_4 = get_sub_field('select-page_4');
                    $select_link_5 = get_sub_field('select-page_5');
                    $link_externo = get_sub_field('link_externo');

                    ?>
                    <div class="col-sm-2">
                        <a href="<?php echo $select_link_1 ?>">
                            <div class="div-svg">
                            <svg fill="none" height="21" viewBox="0 0 24 21" width="24" xmlns="http://www.w3.org/2000/svg"><path clip-rule="evenodd" d="m9.23084 2.76923c0 1.52941-1.23982 2.76923-2.76923 2.76923-1.5294 0-2.76923-1.23982-2.76923-2.76923 0-1.5294 1.23983-2.76923 2.76923-2.76923 1.52941 0 2.76923 1.23983 2.76923 2.76923zm1.75376.9231c.1846-.93231.0831-1.90154-.2954-2.769234h12.3877l-2.7692 2.769234zm2.8615 9.23077c1.5294 0 2.7693-1.2398 2.7693-2.7692 0-1.52943-1.2399-2.76926-2.7693-2.76926s-2.7692 1.23983-2.7692 2.76926c0 1.5294 1.2398 2.7692 2.7692 2.7692zm-13.8461-1.8461v-2.76926h9.61846c-.37846.86769-.48923 1.83696-.29538 2.76926zm6.46161 9.2307c1.52941 0 2.76923-1.2398 2.76923-2.7692s-1.23982-2.7692-2.76923-2.7692c-1.5294 0-2.76923 1.2398-2.76923 2.7692s1.23983 2.7692 2.76923 2.7692zm4.52299-1.8462c.1846-.9323.0831-1.9015-.2954-2.7692h12.3877l-2.7692 2.7692z" fill="#028484" fill-rule="evenodd"/></svg>
                            </div>
                            <p class="disclosure"><?php echo $title_1; ?></p>
                        </a>
                    </div>
                    <div class="col-sm-2">
                        <a href="<?php echo $select_link_2 ?>">
                            <div class="bbva-coronita_rewards"></div>
                            <p class="disclosure"><?php echo $title_2; ?></p>
                        </a>
                    </div>
                    <div class="col-sm-2">
                        <a href="<?php echo $select_link_3 ?>">
                            <div class="bbva-coronita_id"></div>
                            <p class="disclosure"><?php echo $title_3; ?></p>
                        </a>
                    </div>
                    <div class="col-sm-2">
                        <a href="<?php echo $select_link_4 ?>">
                            <div class="div-svg">
                                <svg width="20" height="23" viewBox="0 0 20 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M15.9889 9.3C15.3893 9.1 14.6898 9 13.9903 9C9.59335 9 5.99584 12.6 5.99584 17C5.99584 18.5 6.39557 19.8 7.09508 21H0.999307C0.399723 21 0 20.6 0 20V1C0 0.4 0.399723 0 0.999307 0H14.9896C15.5892 0 15.9889 0.4 15.9889 1V9.3ZM2.99792 7V9H5.99584L7.99446 7H2.99792ZM2.99792 3V5H9.99307L11.9917 3H2.99792ZM13.9903 23C10.6926 23 7.99446 20.3 7.99446 17C7.99446 13.7 10.6926 11 13.9903 11C17.288 11 19.9861 13.7 19.9861 17C19.9861 20.3 17.288 23 13.9903 23ZM12.3914 17.8L11.7918 20L13.9903 18.8L16.1888 20L15.5892 17.8L17.4879 16.3L14.9896 16.1L13.9903 14L12.991 16.1L10.4927 16.3L12.3914 17.8Z" fill="#028484"/>
                                </svg>
                            </div>
                            <p class="disclosure"><?php echo $title_4; ?></p>
                        </a>
                    </div>

                    <div class="col-sm-2">
                        <a href="<?php echo $select_link_5 ?>">
                            <div class="div-svg">
                                <svg width="18" height="24" viewBox="0 0 18 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M1 0H17C17.55 0 18 0.45 18 1V23C18 23.55 17.55 24 17 24H1C0.45 24 0 23.55 0 23V1C0 0.45 0.45 0 1 0ZM8 22C8 22.55 8.45 23 9 23C9.55 23 10 22.55 10 22C10 21.45 9.55 21 9 21C8.45 21 8 21.45 8 22ZM2 20H16V3H2V20ZM3.99001 12H14V14H3.99001V12ZM14 5H3.98001V10H14V5ZM3.99001 16H14V18H3.99001V16Z" fill="#028484"/>
                                </svg>
                            </div>
                            <p class="disclosure"><?php echo $title_5; ?></p>
                        </a>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>

    <!-- Procesos -->
    <?php
    $titulo = get_field('titulo');
    ?>
    <div class="container-fluid bg-bbva-container">
        <h2 class="thin text-center title-process pb30"><?php echo $titulo; ?></h2>
        <div class="container container-procesos">
            <div class="row">
                <div class="col-md-4  col-sm-4 bg-one">
                    <div class="card-text">
                        <?php if (have_rows('card_1')) :
                            while (have_rows('card_1')) : the_row();
                                $title_1 = get_sub_field('title-1');
                                $description = get_sub_field('description-1');
                                $image_1 = get_sub_field('image-1');
                                $url_direction = get_sub_field('url_direction');
                                ?>
                                <a href="<?php echo $url_direction; ?>">
                                    <div class="row svg">
                                        <img class="img-procesos" src="<?php echo $image_1['url']; ?>" alt="<?php echo $image_1['alt']; ?>" />
                                    </div>
                                    <div class="text-card">
                                        <p class="title-cards-colors"><?php echo $title_1; ?></p>
                                        <p class="text-white"> <?php echo $description; ?></p>
                                    </div>
                                </a>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-md-4  col-sm-4 bg-two">
                    <div class="card-text">
                        <?php if (have_rows('card_2')) :
                            while (have_rows('card_2')) : the_row();
                                $title_2 = get_sub_field('title_2');
                                $description_2 = get_sub_field('description_2');
                                $image_2 = get_sub_field('image_2');
                                $url_direction = get_sub_field('url_direction');
                                ?>
                                <a href="<?php echo $url_direction; ?>">
                                    <div class="row svg">
                                        <img class="img-procesos" src="<?php echo $image_2['url']; ?>" alt="<?php echo $image_2['alt']; ?>" />
                                    </div>
                                    <div class="text-card">

                                        <p class="title-cards-colors"><?php echo $title_2; ?></p>

                                        <p class="text-white"> <?php echo $description_2; ?></p>
                                    </div>
                                </a>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 bg-three">
                    <div class="card-text">
                        <?php if (have_rows('card_3')) :
                            while (have_rows('card_3')) : the_row();
                                $title_3 = get_sub_field('title_3');
                                $description_3 = get_sub_field('description_3');
                                $image_3 = get_sub_field('image_3');
                                $url_direction = get_sub_field('url_direction');
                                ?>
                                <a href="<?php echo $url_direction; ?>">
                                    <div class="row svg">
                                        <img class="img-procesos" src="<?php echo $image_3['url']; ?>" alt="<?php echo $image_3['alt']; ?>" />
                                    </div>
                                    <div class="text-card">
                                        <p class="title-cards-colors"><?php echo $title_3; ?></p>
                                        <p class="text-white"> <?php echo $description_3; ?></p>
                                    </div>
                                </a>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="row pb40">
                <div class="col-md-4 col-sm-4 bg-four">
                    <div class="card-text">
                        <?php if (have_rows('card_4')) :
                            while (have_rows('card_4')) : the_row();
                                $title_4 = get_sub_field('title_4');
                                $description_4 = get_sub_field('description_4');
                                $image_4 = get_sub_field('image_4');
                                $url_direction = get_sub_field('url_direction');
                                ?>
                                <a href="<?php echo $url_direction; ?>">
                                    <div class="row svg">
                                        <img class="img-procesos" src="<?php echo $image_4['url']; ?>" alt="<?php echo $image_4['alt']; ?>" />
                                    </div>
                                    <div class="text-card">
                                        <p class="title-cards-colors"><?php echo $title_4; ?></p>
                                        <p class="text-white"><?php echo $description_4; ?></p>
                                    </div>
                                </a>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 bg-five">
                    <div class="card-text">
                        <?php if (have_rows('card_5')) :
                            while (have_rows('card_5')) : the_row();
                                $title_5 = get_sub_field('title_5');
                                $description_5 = get_sub_field('description_5');
                                $image_5 = get_sub_field('image_5');
                                $url_direction = get_sub_field('url_direction');
                                ?>
                                <a href="<?php echo $url_direction; ?>">
                                    <div class="row svg">
                                        <img class="img-procesos" src="<?php echo $image_5['url']; ?>" alt="<?php echo $image_5['alt']; ?>" />
                                    </div>
                                    <div class="text-card">
                                        <p class="title-cards-colors"><?php echo $title_5; ?></p>
                                        <p class="text-white"><?php echo $description_5; ?></p>
                                    </div>
                                </a>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 bg-six">
                    <div class="card-text">
                        <?php if (have_rows('card_6')) :
                            while (have_rows('card_6')) : the_row();
                                $title_6 = get_sub_field('title_6');
                                $description_6 = get_sub_field('description_6');
                                $image_6 = get_sub_field('image_6');
                                $url_direction = get_sub_field('url_direction');
                                ?>
                                <a href="<?php echo $url_direction; ?>">
                                    <div class="row svg">
                                        <img class="img-procesos" src="<?php echo $image_6['url']; ?>" alt="<?php echo $image_6['alt']; ?>" />
                                    </div>
                                    <div class="text-card">
                                        <p class="title-cards-colors"><?php echo $title_6; ?></p>
                                        <p class="text-white"><?php echo $description_6; ?></p>
                                    </div>
                                </a>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- End Procesos -->
     <!-- Sección de Noticias -->
    <div class="featuredArticles">
        <div class="articles">
            <h1 class="text-center thin pt40"> Últimas noticias </h1>
            <div class="bbva-cards bbva-cards-editorial card-stack">
                <div class="container">
                    <?php
                    $queryObject = new Wp_Query(array(
                        'showposts' => 4,
                        'post_type' => array('Noticias1'),
                        'orderby' => 2,
                    ));
                    // The Loop
                    if ($queryObject->have_posts()) :
                        $i = 0;
                        while ($queryObject->have_posts()) :
                            $queryObject->the_post();
                            ?>
                            
                            
                            
                            <?php if ($i == 0) : ?>
                               
                                <div class="row">
                                    <section class="card-block">
                                        <div class="col-xs-12 col-md-6">
                                           
                                <?php
                                  $enlace_imagen = get_field('link_image');
                                  $imagen = get_field('imagen');

                                            if (!empty($enlace_imagen)) {
                                                $url_imagen = $enlace_imagen;
                                            } else {
                                                $url_imagen = $imagen;
                                            }
                                ?>
                                           
                                            <a href="<?php echo the_field('link'); ?>" class="card-wrap" target="_blank">
                                                <span class="card-img bg-grey300" style="background-image: url(<?php echo $url_imagen; ?>)"> </span>
                                                <div class="card-text">

                                                    <?php
                                                                                                        
                                                     
                                                                             
                                                    $titulo = get_the_title(); //Obtener el titulo
                                                                echo '<h3 class="h5">' . substr($titulo, 0, 46) . '</h3>'; //Limitar a 80 caracteres
                                                                ?>
                                                    <?php $description = get_field('description'); ?>
                                                    <?php
                                                                echo '<p class="disclosure text-white">' . $description . '</p>';
                                                                ?>
                                                </div>
                                            </a>
                                        </div>
                                    <?php endif;
                                            if ($i == 1) : ?>
                                            
                                       <?php
                                  $enlace_imagen1 = get_field('link_image');
                                  $imagen1 = get_field('imagen');

                                            if (!empty($enlace_imagen1)) {
                                                $url_imagen1 = $enlace_imagen1;
                                            } else {
                                                $url_imagen1 = $imagen1;
                                            }
                                ?>
                                        <div class="col-xs-12 col-md-6">
                                            <a href="<?php the_field('link'); ?>" class="card-wrap card-height" target="_blank">
                                                <span class="card-img bg-grey300" style="background-image: url(<?php echo $url_imagen1; ?>)"> </span>
                                                <div class="card-text">
                                                    <h3 class="h5"><?php the_title(); ?></h3>
                                                    <?php $description = get_field('description'); ?>
                                                    <?php
                                                                echo '<p class="disclosure">' . $description . '</p>';
                                                                ?>
                                                </div>
                                            </a>
                                        <?php endif; ?>
                                        <?php
                                                if ($i == 2) : ?>
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6">
                                                    <a href="<?php echo the_field('link'); ?>" target="_blank">
                                                        <div class="card-text-noticias">
                                                            <div class="bbva-coronita_auto"></div>
                                                            <h3 class="h5"><?php the_title(); ?></h3>
                                                            <?php $description = get_field('description'); ?>
                                                            <?php
                                                                        echo '<p class="disclosure">' . $description . '</p>';
                                                                        ?>
                                                        </div>
                                                    </a>
                                                </div>
                                            <?php endif; ?>
                                            <?php if ($i == 3) : ?>
                                                <div class="col-md-6 col-sm-6">
                                                    <a href="<?php echo the_field('link'); ?>" target="_blank">
                                                        <div class="card-text-noticias">
                                                            <div class="bbva-coronita_auto"></div>
                                                            <h3 class="h5"><?php the_title(); ?></h3>
                                                            <?php $description = get_field('description'); ?>
                                                            <?php
                                                                echo '<p class="disclosure">' . $description . '</p>';
                                                            ?>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                <?php $i++;
                                    endwhile;
                                endif;
                                ?>
                                        </div>
                                    </section>
                                </div>
                </div>
            </div>
            <?php
            $post_id = 199;
            ?>

            <a href="<?php the_permalink($post_id) ?>">
                <div class="bbva-coronita_add loadmore-icon">
                    <span class="loadmore">Ver más</span>
                </div>
            </a>
        </div>
        <div class="back-image-lands">
            <img class="image-rot" src="<?php echo get_template_directory_uri(); ?>/img/landscape.png" alt="" />
            <p class="aqua text-d-none" style="margin: 0;padding-bottom: 13px;font-size: 18px; font-weight: 700;position: relative;bottom: 6px; text-align: center !important;">Para disfrutar de una mejor experiencia, por favor gira tu teléfono.</p>
        </div>